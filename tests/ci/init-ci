#!/bin/bash
#
# INIT-CI
#
#   This script clones the repo be tested into a persistent location and then
#   if defined, attempts to merge in upstream branch. This then sets up
#   the merged code to be that which is tested.
#
#   Assumes it is running in the repository
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
set -ex

# source global ci config vars
source ./tests/ci/ci.conf

# check for necessary config vars and set downstream ones
[[ -z $CENTRAL_NAMESPACE ]] && { echo "CENTRAL_NAMESPACE must be defined"; exit 1; }
[[ -z $UPSTREAM_BRANCH ]]   && { echo "UPSTREAM_BRANCH must be defined"; exit 1; }
[[ -z $REPO_PATH ]]         && { echo "REPO_PATH must be defined"; exit 1; }

echo ""
echo "Initializing test repo at $REPO_PATH..."
echo ""

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Fetch changes from central repo
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
export PATH=${REPO_PATH}/CCCma_tools/tools:${PATH}
git sremote $CENTRAL_NAMESPACE central
git sfetch central $UPSTREAM_BRANCH
central_repo=$(git remote -v | grep central | grep push | awk '{print $2}')

#~~~~~~~~~~~~~~~~~~
# Attempt to merge
#~~~~~~~~~~~~~~~~~~
echo ""
echo "Attempting to merge $UPSTREAM_BRANCH from central repo!"
echo ""

# first try to merge submodules
git sfed "git merge --no-edit central/${UPSTREAM_BRANCH}" || submodule_merge_fail=1
if (( submodule_merge_fail == 1 )); then
    echo ""
    echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    echo "FAILURE IN SUBMODULE MERGE OF ${UPSTREAM_BRANCH} FROM ${central_repo}"
    echo "DEVELOPER MUST FIX CONFLICTS!"
    echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    echo ""
    exit 1
fi

# commit submodule changes if there were any
git add --all
staged_objects=$(git diff --name-only --staged)
if [[ -n $staged_objects ]]; then
    git commit -m "Updated $staged_objects"
fi

# update super repo
git merge --no-edit central/${UPSTREAM_BRANCH} || super_repo_merge_fail=1
if (( super_repo_merge_fail == 1 )); then
    echo ""
    echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    echo "FAILURE IN SUPER REPO MERGE OF ${UPSTREAM_BRANCH} FROM ${central_repo}"
    echo "DEVELOPER MUST FIX CONFLICTS!"
    echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    echo ""
    exit 1
fi
