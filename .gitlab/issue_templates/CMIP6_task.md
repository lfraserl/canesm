# Section 1 
#### (must be filled when scheduled - ie before active)

- task goal: (i.e. general reason/goal for performing this task) 
- task definition:

	- bulleted list of work to be performed
	- to ensure that task goal is met 

# Section 2 
#### (must be filled when issue/task becomes active)

- branch name:
- parent branch:
- starting commit:
- tests/requirements):

	- bulleted list of tests to verify that the
	- goals for this task have been attained

# Section 3 
#### (must be filled when closing issue)

- final test commit: (i.e. final commit that satisfied task goals)
- merge branch: (i.e. branch merging code into)
- merge commit: (i.e. specific commit code new code merged with)
- merge tests: 

	- bulleted list of tests to be performed
	- to ensure that added task does not modify 
	- modify behaviour/functionality of "merge commit"
- final commit: (i.e. last commit of task that satisfies all merge tests)