- 2023-05-02: CanESM runs now store run source directories (typically on homeords) under a canesm_runs
                subdirectory. Additionally, the data directories are now also stored under a canesm_runs
                directory.

- 2023-05-02: When the model fails, the scripting no longer creates a 'model_failures' directory, and instead
                just always saves the stdout/stderr files. This was done to allow developers to keep old
                failure outputs.

- 2023-01-31: Alter maestro temporary file cleanup so there is a task for each loop and reduced
                the default lage time to 0, so loops are cleaned up after they complete successfully,
                saving their necessary output files.

- 2022-09-08: Static allocation of the physics arrays in CanAM is no longer supported. This is necessary
                to move towards more runtime configuration of the model both within the spectral and future
                GEM versions
- 2022-08-30: To extend a run now, users just need to update 'start_time' and 'stop_time', and re-run
                config-canesm. Once done, they then can submit a whole new experiment. As opposed to the
                old maestro system behaviour, which required users to keep 'start_time' the same, and manually
                submit the next members of each loop.

- 2022-08-12: Model output is no longer streamed to stdout/stderr of the model run job. Instead
                it goes into rank specific, stdout/stderr files in the run directory. Upon completion
                of the model, these files are concatenated together by component and if the model fails,
                linked back to the setup directory, or if the model succeeeds, saved.

- 2022-08-10: User specific databases are no longer supported. If users wish to save and use
                development files, they should be saved to the non-production, global database.
                By using the latest reference environment/develop_canesm, this should be automatically
                handled for users.

- 2022-04-28: Default archiving behaviour updated such that only the restarts are
                archived, and sent to short term. For production runs, archiving of
                everything except the history files are archived on the long term archives

- 2022-04-01: CanESM now uses the maestro sequencer system (exact date of this change is not April 1st)

- 2021-09-01: *final.state files can now be found in their respective component restart bundles

- 2021-09-01: Various scripting/configuration updates:
                
                - ESM/OMIP/AMIP configs now use the same compile-canesm.sh, save_restart_files, 
                  and tapeload_rs scripts
                - the compilation tool chain now gets the make/mkmf templates, along with
                  compilation environment files from 'CONFIG/PLATFORM/' according to
                  the new PLATFORM/COMPILER variables
                - The AGCM now uses a bundled restart file
                - namelists now get routed through a run specific config directory, config/namelists,
                  and can be manually modified by users before launching their run
                    - this means various namelists are no longer saved on $RUNPATH, as the
                      running scripts now pull directly from config/namelists
                - when calling save_restart_files/tapeload_rs, users no longer need to 
                  specify that they would like to use their version of PHYS_PARM or
                  INLINE_DIAG_NL - this happens automatically. 
                    - if users wish to use namelists from the restarts, they can 
                      call these updated scripts with additional scripts (see -h output)
                - component executables now use generic names

- 2021-08-24: PAM has officially been added as a submodule underneath CanAM/external. To keep
              developing on personal forks, developers must fork the new submodule 
              (https://gitlab.science.gc.ca/CanESM/PAM) into their personal namespace. Once 
              this is done, users can update their fork's develop_canesm as normal. However,
              they will need to navigate into 'CanAM' and run git submodule update --init to 
              populate the new submodule and optionally check out the named branch. I.e.
              
                cd CanAM
                git submodule update --init 
                # optional
                cd external/PAM
                git checkout develop_canesm

- 2020-10-21: CanDIAG build system has been significantly overhauled and the Compile_CanDIAG* 
              scripts have all been deprecated and removed. As part of this, when running a 
              simulation, CCRNSRC/executables no longer contains many candiag libraries and now has 
              a bin_diag link that points to the CanDIAG/build directory.

              As part of this change, since the whole library structure of CanDIAG has been changed, 
              the one off compilation tool, 'nogo', has been deprecated and replaced with 'make-diag-pgm'

- 2020-10-02: AGCM compilation is changed slightly, executable get puts in `build/bin`. Initial
              alpha implementation for GNU compiler and containers is added.

- 2020-09-28: AGCM and Coupler executables are no longer stored on $RUNPATH
              and instead are stored in $CCRNSRC/executables.

- 2020-09-25: Ability has been added to checksum AGCM arrays, for reproducibility
              tests.
