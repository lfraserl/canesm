stages:
    - init
    - lint
    - setup
    - build
    - run
    - compare_outputs
    - cleanup

# NOTE: we generally use tests/ci/ci.conf for global config vars, but we need to
#   define SOURCE_REPO_STORAGE_DIR here as we need to first clone the code there
#
#   We manually handle the repo cloning like this, so we can keep a persistent over
#   the course of the run, and test merging easily there. Additionally, it means
#   we only do git operations in the init stage. After that, each stage procedes
#   using the already cloned code
variables:
    GIT_STRATEGY: none # we manually handle the source repo in a persistent location
    SOURCE_REPO_STORAGE_DIR: '/home/scrd105/CanESM-CI-Repos'

# Setup the source repository
init:
    stage: init
    script:
        - rm -rf ci_log* # first clean old pipeline ci logs
        - REPO_PATH=${SOURCE_REPO_STORAGE_DIR}/ci-${CI_COMMIT_SHA:0:7}-CanESM-Repo
        - git clone $CI_REPOSITORY_URL $REPO_PATH
        - cd $REPO_PATH
        - git checkout $CI_COMMIT_SHA
        - git submodule update --init --recursive
        - source ./tests/ci/init-ci 2> ${CI_PROJECT_DIR}/ci_log_${CI_JOB_NAME}_${CI_COMMIT_SHA:0:7}_stderr.txt
    artifacts:
        when: always
        paths:
            - ci_log_${CI_JOB_NAME}_${CI_COMMIT_SHA:0:7}_stderr.txt
        expire_in: 1 week
    only:
        - develop_canesm
        - triggers

# Make sure CanAM passes linter standards
lint:
    stage: lint
    script:
        - REPO_PATH=${SOURCE_REPO_STORAGE_DIR}/ci-${CI_COMMIT_SHA:0:7}-CanESM-Repo
        - cd $REPO_PATH
        - source ./tests/ci/lint-ci
    only:
        - develop_canesm
        - triggers

# template for setting up test runs
.setup_template: &setup_definition
    stage: setup
    script:
        - REPO_PATH=${SOURCE_REPO_STORAGE_DIR}/ci-${CI_COMMIT_SHA:0:7}-CanESM-Repo
        - cd $REPO_PATH
        - source ./tests/ci/setup-ci 2> ${CI_PROJECT_DIR}/ci_log_${CI_JOB_NAME}_${CI_COMMIT_SHA:0:7}_stderr.txt
    artifacts:
        when: always
        paths:
            - ci_log_${CI_JOB_NAME}_${CI_COMMIT_SHA:0:7}_stderr.txt
        expire_in: 1 week
    only:
        - develop_canesm
        - triggers

# template for building the executables
.build_template: &build_definition
    stage: build
    script:
        - REPO_PATH=${SOURCE_REPO_STORAGE_DIR}/ci-${CI_COMMIT_SHA:0:7}-CanESM-Repo
        - cd $REPO_PATH
        - source ./tests/ci/build-ci 2> ${CI_PROJECT_DIR}/ci_log_${CI_JOB_NAME}_${CI_COMMIT_SHA:0:7}_stderr.txt
    artifacts:
        when: always
        paths:
            - ci_logs_compilation_logs_${CI_JOB_NAME}_${CI_COMMIT_SHA:0:7}
            - ci_log_${CI_JOB_NAME}_${CI_COMMIT_SHA:0:7}_stderr.txt
        expire_in: 1 week
    only:
        - develop_canesm
        - triggers

# template for running the model
.run_template: &run_definition
    stage: run
    script:
        - REPO_PATH=${SOURCE_REPO_STORAGE_DIR}/ci-${CI_COMMIT_SHA:0:7}-CanESM-Repo
        - cd $REPO_PATH
        - source ./tests/ci/run-ci 2> ${CI_PROJECT_DIR}/ci_log_${CI_JOB_NAME}_${CI_COMMIT_SHA:0:7}_stderr.txt
    artifacts:
        when: always
        paths:
            - ci_logs_run_output_${CI_JOB_NAME}_${CI_COMMIT_SHA:0:7}
            - ci_log_${CI_JOB_NAME}_${CI_COMMIT_SHA:0:7}_stderr.txt
        expire_in: 1 week
    only:
        - develop_canesm
        - triggers

# template for comparing answers
.compare_answers_template: &compare_answers_definition
    stage: compare_outputs
    script:
        - REPO_PATH=${SOURCE_REPO_STORAGE_DIR}/ci-${CI_COMMIT_SHA:0:7}-CanESM-Repo
        - cd $REPO_PATH
        - source ./tests/ci/comp-answers-ci 2> ${CI_PROJECT_DIR}/ci_log_${CI_JOB_NAME}_${CI_COMMIT_SHA:0:7}_stderr.txt
    artifacts:
        when: always
        paths:
            - ci_logs_run_answers_${CI_JOB_NAME}_${CI_COMMIT_SHA:0:7}
            - ci_log_${CI_JOB_NAME}_${CI_COMMIT_SHA:0:7}_stderr.txt
        expire_in: 1 week
    only:
        - develop_canesm
        - triggers

# clean up job
clean_up:
    stage: cleanup
    script:
        - REPO_PATH=${SOURCE_REPO_STORAGE_DIR}/ci-${CI_COMMIT_SHA:0:7}-CanESM-Repo
        - cd $REPO_PATH
        - source ./tests/ci/cleanup-ci 2> ${CI_PROJECT_DIR}/ci_log_${CI_JOB_NAME}_${CI_COMMIT_SHA:0:7}_stderr.txt
    artifacts:
        when: always
        paths:
            - ci_log_${CI_JOB_NAME}_${CI_COMMIT_SHA:0:7}_stderr.txt
        expire_in: 1 week
    only:
        - develop_canesm
        - triggers

#~~~~~~~~~~~~~~~~~~~~
# ESM-PICONTROL TEST
#~~~~~~~~~~~~~~~~~~~~
.ESM-piControl_config_template: &ESM-piControl_config
    variables:
        IDENTIFIER: pict
        RUN_CONFIG: ESM
        RUNMODE: NEMO-AGCM-pi
        START_YEAR: 6000
        RESTART_RUNID: v510-pict
        RESTART_BRANCH_TIME: 5999_m12

setup-ESM-piControl:
    <<: *ESM-piControl_config
    <<: *setup_definition

build-ESM-piControl:
    <<: *ESM-piControl_config
    <<: *build_definition

run-ESM-piControl:
    <<: *ESM-piControl_config
    <<: *run_definition

comp-ans-ESM-piControl:
    <<: *ESM-piControl_config
    <<: *compare_answers_definition

#~~~~~~~~~~~~~~~~~~~~~
# ESM-HISTORICAL TEST
#~~~~~~~~~~~~~~~~~~~~~
.ESM-historical_config_template: &ESM-historical_config
    variables:
        IDENTIFIER: hist
        RUN_CONFIG: ESM
        RUNMODE: NEMO-AGCM-hist
        START_YEAR: 1850
        RESTART_RUNID: v510-pict
        RESTART_BRANCH_TIME: 5999_m12

setup-ESM-historical:
    <<: *ESM-historical_config
    <<: *setup_definition

build-ESM-historical:
    <<: *ESM-historical_config
    <<: *build_definition

run-ESM-historical:
    <<: *ESM-historical_config
    <<: *run_definition

comp-ans-ESM-historical:
    <<: *ESM-historical_config
    <<: *compare_answers_definition

#~~~~~~~~~~~~~~~~~
# ESM-SSP585 TEST
#~~~~~~~~~~~~~~~~~
.ESM-ssp585_config_template: &ESM-ssp585_config
    variables:
        IDENTIFIER: ssp585
        RUN_CONFIG: ESM
        RUNMODE: NEMO-AGCM-ssp585
        START_YEAR: 2015
        RESTART_RUNID: v510-pict
        RESTART_BRANCH_TIME: 5999_m12

setup-ESM-ssp585:
    <<: *ESM-ssp585_config
    <<: *setup_definition

build-ESM-ssp585:
    <<: *ESM-ssp585_config
    <<: *build_definition

run-ESM-ssp585:
    <<: *ESM-ssp585_config
    <<: *run_definition

comp-ans-ESM-ssp585:
    <<: *ESM-ssp585_config
    <<: *compare_answers_definition

#~~~~~~~~~~~~~~~~~~~~~
# ESM-FCO2-HIST TEST
#~~~~~~~~~~~~~~~~~~~~
.ESM-fco2-hist_config_template: &ESM-fco2-hist_config
    variables:
        IDENTIFIER: fco2-hist
        RUN_CONFIG: ESM
        RUNMODE: NEMO-AGCM-hist-free-co2
        START_YEAR: 1850
        RESTART_RUNID: v510-pict
        RESTART_BRANCH_TIME: 5999_m12

setup-ESM-fco2-hist:
    <<: *ESM-fco2-hist_config
    <<: *setup_definition

build-ESM-fco2-hist:
    <<: *ESM-fco2-hist_config
    <<: *build_definition

run-ESM-fco2-hist:
    <<: *ESM-fco2-hist_config
    <<: *run_definition

comp-ans-ESM-fco2-hist:
    <<: *ESM-fco2-hist_config
    <<: *compare_answers_definition

#~~~~~~~~~~~~~~~~~~~~~
# ESM-DCPP-ASSIM TEST
#~~~~~~~~~~~~~~~~~~~~~
.ESM-dcpp-asm_config_template: &ESM-dcpp-asm_config
    variables:
        IDENTIFIER: dcpp-asm
        RUN_CONFIG: ESM
        RUNMODE: NEMO-AGCM-dcpp-assim
        START_YEAR: 1958
        RESTART_RUNID: v510-pict
        RESTART_BRANCH_TIME: 5999_m12

setup-ESM-dcpp-asm:
    <<: *ESM-dcpp-asm_config
    <<: *setup_definition

build-ESM-dcpp-asm:
    <<: *ESM-dcpp-asm_config
    <<: *build_definition

run-ESM-dcpp-asm:
    <<: *ESM-dcpp-asm_config
    <<: *run_definition

comp-ans-ESM-dcpp-asm:
    <<: *ESM-dcpp-asm_config
    <<: *compare_answers_definition

#~~~~~~~~~~~~~~~
# AMIP-DEV TEST
#~~~~~~~~~~~~~~~
.AMIP-dev_config_template: &AMIP-dev_config
    variables:
        IDENTIFIER: adev
        RUN_CONFIG: AMIP
        RUNMODE: CanAM-Dev-2003-2008
        START_YEAR: 2003
        RESTART_RUNID: restart-canesm5d1rc1-01
        RESTART_BRANCH_TIME: 0001_m12

setup-AMIP-dev:
    <<: *AMIP-dev_config
    <<: *setup_definition

build-AMIP-dev:
    <<: *AMIP-dev_config
    <<: *build_definition

run-AMIP-dev:
    <<: *AMIP-dev_config
    <<: *run_definition

comp-ans-AMIP-dev:
    <<: *AMIP-dev_config
    <<: *compare_answers_definition

#~~~~~~~~~~~~~~~~~~
# AMIP-MAMDEV TEST
#~~~~~~~~~~~~~~~~~~
.AMIP-mamdev_config_template: &AMIP-mamdev_config
    variables:
        IDENTIFIER: mamadev
        RUN_CONFIG: AMIP
        RUNMODE: CanAM-MAM-Dev-2003-2008
        START_YEAR: 2003
        RESTART_RUNID: maminit-v510-26d1
        RESTART_BRANCH_TIME: 0001_m12

setup-AMIP-mamdev:
    <<: *AMIP-mamdev_config
    <<: *setup_definition

build-AMIP-mamdev:
    <<: *AMIP-mamdev_config
    <<: *build_definition

run-AMIP-mamdev:
    <<: *AMIP-mamdev_config
    <<: *run_definition

comp-ans-AMIP-mamdev:
    <<: *AMIP-mamdev_config
    <<: *compare_answers_definition

#~~~~~~~~~~~
# OMIP TEST
#~~~~~~~~~~~
.OMIP_config_template: &OMIP_config
    variables:
        IDENTIFIER: omip
        RUN_CONFIG: OMIP
        RUNMODE: OMIP
        START_YEAR: 1700
        RESTART_RUNID: omip-cmoc08
        RESTART_BRANCH_TIME: 1699_m12

setup-OMIP:
    <<: *OMIP_config
    <<: *setup_definition

build-OMIP:
    <<: *OMIP_config
    <<: *build_definition

run-OMIP:
    <<: *OMIP_config
    <<: *run_definition

comp-ans-OMIP:
    <<: *OMIP_config
    <<: *compare_answers_definition
