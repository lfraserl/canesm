#!/bin/bash
#
#   RUN CI
#   
#       This script takes run-ci.pbs template
#       and modifies the resources as necessary
#       and then submits it to the proper machines
#
# NOTE: 
#   for now we use the same cpu request as that is used in
#   the maestro system, which works fine because we currently
#   source model_run.tsk, but after verifying that we get proper
#   results with this, we should make it so we make a more fine-grained
#   pbs request (i.e. something that won't require task pinning) and
#   avoid the use of model_run.task
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
set -ex

# source global ci config vars
source ./tests/ci/ci.conf

# check for necessary config vars and set downstream ones
[[ -z $IDENTIFIER ]]                && { echo "IDENTIFIER must be defined"; exit 1; }
[[ -z $TEST_RUN_DIR ]]              && { echo "TEST_RUN_DIR must be defined"; exit 1; }
[[ -z $RUN_MACH ]]                  && { echo "RUN_MACH must be defined"; exit 1; }
[[ -z $DAYS_TO_RUN ]]               && { echo "DAYS_TO_RUN must be defined"; exit 1; }
[[ -z $COMPUTE_SYSTEM ]]            && { echo "COMPUTE_SYSTEM must be defined"; exit 1; }
[[ -z $CI_SCRATCH_SPACE ]]          && { echo "CI_SCRATCH_SPACE must be defined"; exit 1; }
RUNID=ci-${IDENTIFIER}-${CI_COMMIT_SHA:0:7}
RUN_DIRECTORY=${TEST_RUN_DIR}/${RUNID}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Determine run parameters/qsub arguments
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
cd $RUN_DIRECTORY
source env_setup_file 2> /dev/null
source config/canesm-shell-params.sh 2> /dev/null
source ${CANESM_SRC_ROOT}/CCCma_tools/tools/CanESM_shell_functions.sh 2> /dev/null
backend_procs_per_node=$(grep -Po --color=never '(?<=BACKEND_PROC_PER_NODE=).*' config/${COMPUTE_SYSTEM}.compute_system.cfg)
resource_request="-l select=${phys_node}:ncpus=${backend_procs_per_node}:mem=160G -l walltime=00:30:00"    # see note above about resources
job_start_date=$run_start_date
job_stop_date=$(increment_day $job_start_date $(( DAYS_TO_RUN - 1)) )
job_name=${RUNID}-model-run
job_stderr=${CI_SCRATCH_SPACE}/${job_name}_err.txt
job_stdout=${CI_SCRATCH_SPACE}/${job_name}_out.txt
WRK_DIR=${RUN_DIRECTORY}
SCRATCH_SPACE=${CI_SCRATCH_SPACE}/${job_name}
variables_to_pass_job="SCRATCH_SPACE,WRK_DIR,job_start_date,job_stop_date"

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Bring pbs script to remote mach and submit!
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
unique_jobscript=${job_name}.run-ci.pbs
scp ${CANESM_SRC_ROOT}/tests/ci/run-ci.pbs $(whoami)@${RUN_MACH}:~/${unique_jobscript}

# ssh, export the environment vars (so they can be passed to the job) and submit
echo ""
echo "Submitting test run to ${RUN_MACH}..."
echo ""
ssh $RUN_MACH << EOF
    set -e
    export WRK_DIR=${WRK_DIR}
    export job_start_date=${job_start_date}
    export job_stop_date=${job_stop_date}
    export SCRATCH_SPACE=${SCRATCH_SPACE}
    qsub -N $job_name -o $job_stdout -e $job_stderr -v $variables_to_pass_job $resource_request ${unique_jobscript} > ${unique_jobscript}.jobid
    rm ${unique_jobscript}
EOF

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Monitor job for completion, pull logs, and check status
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
function is_job_running(){
    local _job_id=$1
    local _mach=$2 
    local _return_status
    ssh ${_mach} << EOF
        set -e
        qstat ${_job_id}
EOF
    _return_status=$?
    return ${_return_status}
}
function is_string_in_file(){
    local _string=$1
    local _file=$2
    local _return_status=1
    grep -q "${_string}" ${_file} && _return_status=0
    return ${_return_status}
}

# wait for job to end
job_id=$(ssh ${RUN_MACH} "cat ${unique_jobscript}.jobid; rm ${unique_jobscript}.jobid")
echo ""
echo "Monitoring $job_id on $RUN_MACH..."
echo ""
while true; do
    if ! is_job_running ${job_id} ${RUN_MACH}; then
        break
    fi
    sleep 15
done
echo ""
echo "$job_id complete! Checking status..."
echo ""

# pull job stderr/stdout and check for "CI run failed/successful"
output_storage_dir=${CI_PROJECT_DIR}/ci_logs_run_output_${CI_JOB_NAME}_${CI_COMMIT_SHA:0:7}
stored_job_stderr=${output_storage_dir}/$(basename $job_stderr)
stored_job_stdout=${output_storage_dir}/$(basename $job_stdout)
rm -rf $output_storage_dir
mkdir $output_storage_dir
scp $(whoami)@${RUN_MACH}:${job_stderr} ${stored_job_stderr}
scp $(whoami)@${RUN_MACH}:${job_stdout} ${stored_job_stdout}
if is_string_in_file "CI run failed" $stored_job_stdout; then
    echo ""
    echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    echo "CI run failed! Check logs (in the artifacts!)"     
    echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    echo ""
    exit 1
elif is_string_in_file "CI run successful" $stored_job_stdout; then
    echo ""
    echo "CI run successful!"
    echo ""
else
    echo ""
    echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    echo "UNABLE TO DETERMINE JOB STATUS!"
    echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    echo ""
    exit 1
fi

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Store results in setup directory
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# get the final state files
stored_answer_dir=${RUN_DIRECTORY}/answers
mkdir $stored_answer_dir
echo ""
echo "Storing answers at ${stored_answer_dir}..."
echo ""
scp $(whoami)@${RUN_MACH}:${SCRATCH_SPACE}/*final.state ${stored_answer_dir}/

# rename them with the run duration for clarity
for f in $(ls ${stored_answer_dir}/*final.state); do
    mv ${f} ${f}.${RUNID}.${DAYS_TO_RUN}days
done
echo "Stored!"

echo ""
echo "Run stage successful!"
echo ""
